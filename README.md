# HOMEWORK 2 #

### REQUIREMENTS ###

Cloudera Quickstart VM 5.12.0, Apache Spark 2.1.0, 4+ GiB RAM

### SET UP ###

```sh
git clone  git@bitbucket.org:rvalexandrov/dsbda_homework2.git
cd dsbda_homework2
mvn clean install
```

### INPUT DATA ###

```sh
java -jar dsbda_hw2-1.0-SNAPSHOT-jar-with-dependencies.jar generate
chmod +x prepare_data.sh
./prepare_data.sh
```

### RUN ###

```sh
cd target
java -jar dsbda_hw2-1.0-SNAPSHOT-jar-with-dependencies.jar processing
```
