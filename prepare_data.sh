#/bin/bash

# change to hdfs commands
hadoop fs -mkdir -p input;
echo "Directories were created!"


hadoop fs -put target/inputSalary.json /input;
hadoop fs -put target/inputTrips.json /input;

hadoop fs -ls /input

echo "All files were copied to HDFS"